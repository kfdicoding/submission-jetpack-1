package com.dicoding.motalog.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.dicoding.motalog.data.SampleData
import com.dicoding.motalog.databinding.ItemShowBinding

class ItemShowAdapter(
        private val listItem: List<SampleData.ItemShow>,
        private val action:(position:Int)-> Unit
):RecyclerView.Adapter<ItemShowAdapter.ItemShowViewHolder>() {

    inner class ItemShowViewHolder(@NonNull val binding: ItemShowBinding):RecyclerView.ViewHolder(binding.root){

        fun bind(item: SampleData.ItemShow){
            binding.mTitleItem.text = item.title
            binding.mImageCover.setImageResource(item.cover)
            binding.mDateItem.text = item.release
            binding.mSmallDesc.text = item.description
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemShowViewHolder {
        return ItemShowViewHolder(ItemShowBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ItemShowViewHolder, position: Int) {
        holder.bind(listItem[position])
        holder.itemView.setOnClickListener {
            action(position)
        }
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

}