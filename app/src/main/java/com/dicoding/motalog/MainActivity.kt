package com.dicoding.motalog

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.dicoding.motalog.data.SampleData
import com.dicoding.motalog.fragments.FragmentDetailItemShow
import com.dicoding.motalog.fragments.MainFragment
import com.dicoding.motalog.viewModels.MoviesViewModel
import com.dicoding.motalog.viewModels.TvViewModel

class MainActivity : AppCompatActivity() {

    private val movieViewModel by viewModels<MoviesViewModel>()
    private val tvViewModel by viewModels<TvViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.containerFragment, MainFragment(), MainFragment::class.qualifiedName)
                .commit()
    }

    fun replaceFragment(type: String){
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.containerFragment, FragmentDetailItemShow.mOnSavedInstanceFragment(type), FragmentDetailItemShow::class.qualifiedName)
                .addToBackStack(null)
                .commit()
    }

    fun getViewModelTV() = tvViewModel
    fun getViewModelMovie() = movieViewModel
}