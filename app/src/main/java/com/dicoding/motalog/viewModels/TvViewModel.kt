package com.dicoding.motalog.viewModels

import androidx.lifecycle.ViewModel
import com.dicoding.motalog.data.SampleData

class TvViewModel: ViewModel() {

    var tvItems:List<SampleData.ItemShow> = emptyList()
    var tv: SampleData.ItemShow = SampleData.ItemShow()

    fun initTvsData(){
        tvItems = SampleData.generateTvData()
    }

    fun setTvSelected(index: Int){
        tv = tvItems[index]
    }

}