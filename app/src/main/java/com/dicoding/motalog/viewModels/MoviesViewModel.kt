package com.dicoding.motalog.viewModels

import androidx.lifecycle.ViewModel
import com.dicoding.motalog.data.SampleData

class MoviesViewModel: ViewModel() {

    var movieItems:List<SampleData.ItemShow> = emptyList()
    var movie: SampleData.ItemShow = SampleData.ItemShow()

    fun initMoviesData(){
        movieItems = SampleData.generateMovieData()
    }

    fun setMovieSelected(index: Int){
        movie = movieItems[index]
    }

}