package com.dicoding.motalog.data

import com.dicoding.motalog.R
import com.google.gson.JsonArray
import com.google.gson.JsonParser

object SampleData {


    data class ItemShow(
            val title: String = "",
            val cover: Int = 0,
            val genre: String = "",
            val score: Int = 0,
            val release: String = "",
            val duration: String = "",
            val description: String = ""
    )

    private val coverMovie = arrayOf(
            R.drawable.poster_movie_a_start_is_born,
            R.drawable.poster_movie_alita,
            R.drawable.poster_movie_aquaman,
            R.drawable.poster_movie_bohemian,
            R.drawable.poster_movie_cold_persuit,
            R.drawable.poster_movie_creed,
            R.drawable.poster_movie_crimes,
            R.drawable.poster_movie_glass,
            R.drawable.poster_movie_how_to_train,
            R.drawable.poster_movie_infinity_war,
            R.drawable.poster_movie_marry_queen,
            R.drawable.poster_movie_master_z
    )

    private val coverTvShow = arrayOf(
            R.drawable.poster_tv_arrow,
            R.drawable.poster_tv_doom_patrol,
            R.drawable.poster_tv_dragon_ball,
            R.drawable.poster_tv_fairytail,
            R.drawable.poster_tv_family_guy,
            R.drawable.poster_tv_flash,
            R.drawable.poster_tv_god,
            R.drawable.poster_tv_gotham,
            R.drawable.poster_tv_grey_anatomy,
            R.drawable.poster_tv_hanna,
            R.drawable.poster_tv_iron_fist,
            R.drawable.poster_tv_the_simpson,
    )

    private val attributeMovie = JsonParser.parseString("""[
            {
                "title": "A Star Is Born",
                "date" : "October 3, 2018",
                "description" : "Seasoned musician Jackson Maine discovers — and falls in love with — struggling artist Ally. She has just about given up on her dream to make it big as a singer — until Jack coaxes her into the spotlight. But even as Ally's career takes off, the personal side of their relationship is breaking down, as Jack fights an ongoing battle with his own internal demons.",
                "popularity" : 75,
                "genre" : "Drama, Romance, Music",
                "duration" : "2h 16m"       
            },
            {
                "title" : "Alita: Battle Angel",
                "date" : "January 31, 2019",
                "description" : "When Alita awakens with no memory of who she is in a future world she does not recognize, she is taken in by Ido, a compassionate doctor who realizes that somewhere in this abandoned cyborg shell is the heart and soul of a young woman with an extraordinary past.",
                "popularity" : 71,
                "genre" : "Action, Science Fiction, Adventure",
                "duration" : "2h 2m"
            },
            {
                "title" : "Aquaman",
                "date" : "December 7, 2018",
                "description" : "Once home to the most advanced civilization on Earth, Atlantis is now an underwater kingdom ruled by the power-hungry King Orm. With a vast army at his disposal, Orm plans to conquer the remaining oceanic people and then the surface world. Standing in his way is Arthur Curry, Orm's half-human, half-Atlantean brother and true heir to the throne.",
                "popularity" : 69,
                "genre" : "Action, Adventure, Fantasy",
                "duration" : "2h 23m"
            },
            {
                "title" : "Bohemian Rhapsody",
                "date" : "October 24, 2018",
                "description" : "Singer Freddie Mercury, guitarist Brian May, drummer Roger Taylor and bass guitarist John Deacon take the music world by storm when they form the rock 'n' roll band Queen in 1970. Hit songs become instant classics. When Mercury's increasingly wild lifestyle starts to spiral out of control, Queen soon faces its greatest challenge yet – finding a way to keep the band together amid the success and excess.",
                "popularity" : 80,
                "genre" : "Music, Drama, History",
                "duration" : "2h 15m"
            },
            {
                "title" : "Cold Pursuit",
                "date" : "February 7, 2019",
                "description" : "The quiet family life of Nels Coxman, a snowplow driver, is upended after his son's murder. Nels begins a vengeful hunt for Viking, the drug lord he holds responsible for the killing, eliminating Viking's associates one by one. As Nels draws closer to Viking, his actions bring even more unexpected and violent consequences, as he proves that revenge is all in the execution.",
                "popularity" : 56,
                "genre" : "Action, Crime, Thriller",
                "duration" : "1h 59m"
            },
            {
                "title" : "Creed II",
                "date" : "November 21, 2018",
                "description" : "Between personal obligations and training for his next big fight against an opponent with ties to his family's past, Adonis Creed is up against the challenge of his life.",
                "popularity" : 69,
                "genre" : "Drama",
                "duration" : "2h 10m"
            },
            {
                "title" : "Fantastic Beasts: The Crimes of Grindelwald",
                "date" : "November 14, 2018",
                "description" : "Gellert Grindelwald has escaped imprisonment and has begun gathering followers to his cause—elevating wizards above all non-magical beings. The only one capable of putting a stop to him is the wizard he once called his closest friend, Albus Dumbledore. However, Dumbledore will need to seek help from the wizard who had thwarted Grindelwald once before, his former student Newt Scamander, who agrees to help, unaware of the dangers that lie ahead. Lines are drawn as love and loyalty are tested, even among the truest friends and family, in an increasingly divided wizarding world.",
                "popularity" : 69,
                "genre" : "Adventure, Fantasy, Drama",
                "duration" : "2h 14m"
            },
            {
                "title" : "Glass",
                "date" : "January 16, 2019",
                "description" : "In a series of escalating encounters, former security guard David Dunn uses his supernatural abilities to track Kevin Wendell Crumb, a disturbed man who has twenty-four personalities. Meanwhile, the shadowy presence of Elijah Price emerges as an orchestrator who holds secrets critical to both men.",
                "popularity" : 66,
                "genre" : "Thriller, Drama, Science Fiction",
                "duration" : "2h 9m"
            },
            {
                "title" : "How to Train Your Dragon: The Hidden World",
                "date" : "January 3, 2019",
                "description" : "As Hiccup fulfills his dream of creating a peaceful dragon utopia, Toothless’ discovery of an untamed, elusive mate draws the Night Fury away. When danger mounts at home and Hiccup’s reign as village chief is tested, both dragon and rider must make impossible decisions to save their kind.",
                "popularity" : 78,
                "genre" : "Animation, Family, Adventure ",
                "duration" : "1h 44m"
            },
            {
                "title" : "Avengers: Infinity War",
                "date" : "April 25, 2018",
                "description" : "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.",
                "popularity" : 83,
                "genre" : "Adventure, Action, Science Fiction",
                "duration" : "2h 29m"
            },
            {
                "title" : "Mary Queen of Scots",
                "date" : "December 7, 2018",
                "description" : "In 1561, Mary Stuart, widow of the King of France, returns to Scotland, reclaims her rightful throne and menaces the future of Queen Elizabeth I as ruler of England, because she has a legitimate claim to the English throne. Betrayals, rebellions, conspiracies and their own life choices imperil both Queens. They experience the bitter cost of power, until their tragic fate is finally fulfilled.",
                "popularity" : 66,
                "genre" : "Drama, History",
                "duration" : "2h 4m"
            },
            {
                "title" : "Master Z: Ip Man Legacy",
                "date" : "December 20, 2018",
                "description" : "Following his defeat by Master Ip, Cheung Tin Chi tries to make a life with his young son in Hong Kong, waiting tables at a bar that caters to expats. But it's not long before the mix of foreigners, money, and triad leaders draw him once again to the fight.",
                "popularity" : 57,
                "genre" : "Action",
                "duration" : "1h 47m"
            },
        ]""".trimIndent()).asJsonArray

    private val attributeTv = JsonParser.parseString("""[
            {
                "title" : "Arrow",
                "date" : "October 10, 2012",
                "description" : "Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow.",
                "popularity" : 0,
                "genre" : "Crime, Drama, Mystery, Action & Adventure",
                "duration" : "42m"
             },
             {
                "title" : "Doom Patrol",
                "date" : "February 15, 2019",
                "description" : "The Doom Patrol’s members each suffered horrible accidents that gave them superhuman abilities — but also left them scarred and disfigured. Traumatized and downtrodden, the team found purpose through The Chief, who brought them together to investigate the weirdest phenomena in existence — and to protect Earth from what they find.",
                "popularity" : 76,
                "genre" : "Sci-Fi & Fantasy, Action & Adventure, Comedy",
                "duration" : "49m"
             },
             {
                "title" : "Dragon Ball",
                "date" : "February 26, 1986",
                "description" : "Long ago in the mountains, a fighting master known as Gohan discovered a strange boy whom he named Goku. Gohan raised him and trained Goku in martial arts until he died. The young and very strong boy was on his own, but easily managed. Then one day, Goku met a teenage girl named Bulma, whose search for the mystical Dragon Balls brought her to Goku's home. Together, they set off to find all seven and to grant her wish.",
                "popularity" : 81,
                "genre" : "Comedy, Sci-Fi & Fantasy, Animation, Action & Adventure",
                "duration" : "25m"
             },
             {
                "title" : "Fairy Tail",
                "date" : "October 12, 2009",
                "description" : "Lucy is a 17-year-old girl, who wants to be a full-fledged mage. One day when visiting Harujion Town, she meets Natsu, a young man who gets sick easily by any type of transportation. But Natsu isn't just any ordinary kid, he's a member of one of the world's most infamous mage guilds: Fairy Tail.",
                "popularity" : 77,
                "genre" : "Action & Adventure, Animation, Comedy, Sci-Fi & Fantasy",
                "duration" : "25m"
             },
             {
                "title" : "Family Guy",
                "date" : "January 31, 1999",
                "description" : "Sick, twisted, politically incorrect and Freakin' Sweet animated series featuring the adventures of the dysfunctional Griffin family. Bumbling Peter and long-suffering Lois have three kids. Stewie (a brilliant but sadistic baby bent on killing his mother and taking over the world), Meg (the oldest, and is the most unpopular girl in town) and Chris (the middle kid, he's not very bright but has a passion for movies). The final member of the family is Brian - a talking dog and much more than a pet, he keeps Stewie in check whilst sipping Martinis and sorting through his own life issues.",
                "popularity" : 69,
                "genre" : "Animation, Comedy",
                "duration" : "22m"
             },
             {
                "title" : "The Flash",
                "date" : "October 7, 2014",
                "description" : "After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \"meta-human\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won't be long before the world learns what Barry Allen has become...The Flash.",
                "popularity" : 76,
                "genre" : "Drama, Sci-Fi & Fantasy",
                "duration" : "44m"
             },
             {
                "title" : "Game of Thrones",
                "date" : "April 17, 2011",
                "description" : "Seven noble families fight for control of the mythical land of Westeros. Friction between the houses leads to full-scale war. All while a very ancient evil awakens in the farthest north. Amidst the war, a neglected military order of misfits, the Night's Watch, is all that stands between the realms of men and icy horrors beyond.",
                "popularity" : 84,
                "genre" : "Sci-Fi & Fantasy, Drama, Action & Adventure",
                "duration" : "1h"
             },
             {
                "title" : "Gotham",
                "date" : "September 22, 2014",
                "description" : "Everyone knows the name Commissioner Gordon. He is one of the crime world's greatest foes, a man whose reputation is synonymous with law and order. But what is known of Gordon's story and his rise from rookie detective to Police Commissioner? What did it take to navigate the multiple layers of corruption that secretly ruled Gotham City, the spawning ground of the world's most iconic villains? And what circumstances created them – the larger-than-life personas who would become Catwoman, The Penguin, The Riddler, Two-Face and The Joker?",
                "popularity" : 75,
                "genre" : "Drama, Crime, Sci-Fi & Fantasy",
                "duration" : "43m"
             },
             {
                "title" : "Grey's Anatomy",
                "date" : "March 27, 2005",
                "description" : "Follows the personal and professional lives of a group of doctors at Seattle’s Grey Sloan Memorial Hospital.",
                "popularity" : 82,
                "genre" : "Drama",
                "duration" : "43m"
             },
             {
                "title" : "Hanna",
                "date" : "March 28, 2019",
                "description" : "This thriller and coming-of-age drama follows the journey of an extraordinary young girl as she evades the relentless pursuit of an off-book CIA agent and tries to unearth the truth behind who she is. Based on the 2011 Joe Wright film.",
                "popularity" : 75,
                "genre" : "Action & Adventure, Drama",
                "duration" : "50m"
             },
             {
                "title" : "Marvel's Iron Fist",
                "date" : "March 17, 2017",
                "description" : "Danny Rand resurfaces 15 years after being presumed dead. Now, with the power of the Iron Fist, he seeks to reclaim his past and fulfill his destiny.",
                "popularity" : 65,
                "genre" : "Action & Adventure, Drama, Sci-Fi & Fantasy",
                "duration" : "55m"
             },
             {
                "title" : "The Simpsons",
                "date" : "December 16, 1989",
                "description" : "Set in Springfield, the average American town, the show focuses on the antics and everyday adventures of the Simpson family; Homer, Marge, Bart, Lisa and Maggie, as well as a virtual cast of thousands. Since the beginning, the series has been a pop culture icon, attracting hundreds of celebrities to guest star. The show has also made name for itself in its fearless satirical take on politics, media and American life in general.",
                "popularity" : 78,
                "genre" : " Family, Animation, Comedy ",
                "duration" : "22m"
             },
    ]""".trimIndent()).asJsonArray


    fun generateMovieData(): List<ItemShow>{
        val items = ArrayList<ItemShow>()

        coverMovie.forEachIndexed { index, cover ->
            val attributeItem = attributeMovie[index].asJsonObject
            items.add(ItemShow(
                    title = attributeItem.getAsJsonPrimitive("title").asString,
                    cover,
                    genre = attributeItem.getAsJsonPrimitive("genre").asString,
                    score = attributeItem.getAsJsonPrimitive("popularity").asInt,
                    release = attributeItem.getAsJsonPrimitive("date").asString,
                    duration = attributeItem.getAsJsonPrimitive("duration").asString,
                    description = attributeItem.getAsJsonPrimitive("description").asString
            ))
        }

        return items
    }

    fun generateTvData(): List<ItemShow>{
        val items = ArrayList<ItemShow>()

        coverTvShow.forEachIndexed { index, cover ->
            val attributeItem = attributeTv[index].asJsonObject
            items.add(ItemShow(
                    title = attributeItem.getAsJsonPrimitive("title").asString,
                    cover,
                    genre = attributeItem.getAsJsonPrimitive("genre").asString,
                    score = attributeItem.getAsJsonPrimitive("popularity").asInt,
                    release = attributeItem.getAsJsonPrimitive("date").asString,
                    duration = attributeItem.getAsJsonPrimitive("duration").asString,
                    description = attributeItem.getAsJsonPrimitive("description").asString
            ))
        }

        return items
    }
}