package com.dicoding.motalog.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.dicoding.motalog.fragments.FragmentItemShow.Companion.TYPE_MOVIE
import com.dicoding.motalog.fragments.FragmentItemShow.Companion.TYPE_TV
import com.dicoding.motalog.databinding.MainFragmentBinding
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainFragment:Fragment() {

    private var binding: MainFragmentBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MainFragmentBinding.inflate(inflater)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpViewPager()
    }

    private val mTabNames = arrayListOf(TYPE_MOVIE, TYPE_TV)
    private fun setUpViewPager(){
        binding?.run {
            mViewPager.adapter = AdapterViewPagerItem(childFragmentManager)
            TabLayoutMediator(mTabLayout,mViewPager){tab, position->
                tab.text = mTabNames[position]
            }.attach()

            mTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
                override fun onTabSelected(tab: TabLayout.Tab?) {

                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }

            })
        }
    }

    inner class AdapterViewPagerItem(
        fm: FragmentManager
    ): FragmentStateAdapter(fm, lifecycle) {

        override fun getItemCount(): Int {
            return mTabNames.size
        }

        override fun createFragment(position: Int): Fragment {
            return FragmentItemShow.mOnSavedInstanceFragment(mTabNames[position])
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

}