package com.dicoding.motalog.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dicoding.motalog.adapters.ItemShowAdapter
import com.dicoding.motalog.MainActivity
import com.dicoding.motalog.databinding.FragmentItemShowBinding

class FragmentItemShow: Fragment() {

    companion object{
        private const val EXTRA_TYPE_ITEM = "extraTypeItem"
        const val TYPE_MOVIE = "Movie"
        const val TYPE_TV = "TV Show"

        fun mOnSavedInstanceFragment(type:String): FragmentItemShow {
            val fragment = FragmentItemShow()
            val mArgs = Bundle()

            mArgs.putString(EXTRA_TYPE_ITEM, type)
            fragment.arguments = mArgs

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        type = if (savedInstanceState != null)
            savedInstanceState.getString(EXTRA_TYPE_ITEM)?:""
        else arguments?.getString(EXTRA_TYPE_ITEM)?:""
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(EXTRA_TYPE_ITEM, type)
        super.onSaveInstanceState(outState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentItemShowBinding.inflate(inflater)
        return binding?.root
    }

    private var binding:FragmentItemShowBinding? = null
    private var type = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setItemShow()
    }

    private fun setItemShow(){
        when(type){
            TYPE_MOVIE -> setUpMovie()
            TYPE_TV -> setUpTv()
        }
    }

    private fun setUpMovie(){
        mainActivity().getViewModelMovie().initMoviesData()
        binding?.mListItemShow?.adapter = ItemShowAdapter(mainActivity().getViewModelMovie().movieItems){position ->
            onClickItem(FragmentDetailItemShow.TYPE_MOVIE_DETAIL) {
                selectMovie(position)
            }
        }
    }

    private fun setUpTv(){
        mainActivity().getViewModelTV().initTvsData()
        binding?.mListItemShow?.adapter = ItemShowAdapter(mainActivity().getViewModelTV().tvItems){position ->
            onClickItem(FragmentDetailItemShow.TYPE_TV_DETAIL) {
                selectTv(position)
            }
        }
    }

    private fun selectMovie(position: Int){
        mainActivity().getViewModelMovie().setMovieSelected(position)
    }

    private fun selectTv(position: Int){
        mainActivity().getViewModelTV().setTvSelected(position)
    }

    private fun onClickItem(type: String,selectItem:()->Unit){
        selectItem()
        mainActivity().replaceFragment(type)
    }

    private fun mainActivity() = requireActivity() as MainActivity

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}