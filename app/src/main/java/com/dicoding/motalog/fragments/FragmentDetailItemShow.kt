package com.dicoding.motalog.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dicoding.motalog.MainActivity
import com.dicoding.motalog.data.SampleData
import com.dicoding.motalog.databinding.FragmentDetailItemShowBinding

class FragmentDetailItemShow: Fragment() {

    companion object{
        private const val EXTRA_TYPE_ITEM_DETAIL = "extraTypeItem"
        const val TYPE_MOVIE_DETAIL = "itemTypeMovie"
        const val TYPE_TV_DETAIL = "itemTypeTv"

        fun mOnSavedInstanceFragment(type:String): FragmentDetailItemShow {
            val fragment = FragmentDetailItemShow()
            val mArgs = Bundle()

            mArgs.putString(EXTRA_TYPE_ITEM_DETAIL, type)
            fragment.arguments = mArgs

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        type = if (savedInstanceState != null)
            savedInstanceState.getString(EXTRA_TYPE_ITEM_DETAIL)?:""
        else arguments?.getString(EXTRA_TYPE_ITEM_DETAIL)?:""
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(EXTRA_TYPE_ITEM_DETAIL, type)
        super.onSaveInstanceState(outState)
    }

    private var binding: FragmentDetailItemShowBinding? = null
    private var type = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentDetailItemShowBinding.inflate(inflater)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val itemData = when(type){
            TYPE_MOVIE_DETAIL -> mainActivity().getViewModelMovie().movie
            TYPE_TV_DETAIL -> mainActivity().getViewModelTV().tv
            else -> SampleData.ItemShow()
        }

        initToolbar(itemData)

        binding?.run {
            itemCover.setImageResource(itemData.cover)
            itemTitle.text = itemData.title
            itemGenre.text = itemData.genre
            itemScore.text = itemData.score.toString()
            itemDuration.text = itemData.duration
            itemDescription.text = itemData.description
        }
    }

    private fun initToolbar(itemData: SampleData.ItemShow) {
        mainActivity().setSupportActionBar(binding?.mToolbar)
        mainActivity().supportActionBar?.setHomeButtonEnabled(true)
        mainActivity().supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding?.mToolbar?.title = itemData.title
        binding?.mToolbar?.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun mainActivity() = requireActivity() as MainActivity
}