package com.dicoding.motalog.viewModels

import com.dicoding.motalog.data.SampleData
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
/*
 *
 * skenario test :
 *
 * memuat data movie:
 *     - memastikan movie tidak null
 *     - memastikan jumlah data movie sesuai dengan yang diharapkan
 *     - memastikan data movie yang dipilih sesuai
 */

class MoviesViewModelTest {

    private lateinit var viewModel: MoviesViewModel
    private val movieSelectedSample = SampleData.generateMovieData()[0]
    private val indexSelected = 0

    @Before
    fun setUp(){
        viewModel = MoviesViewModel()
        viewModel.initMoviesData()
        viewModel.setMovieSelected(indexSelected)
    }

    @Test
    fun getMovieItems() {
        val movies = viewModel.movieItems
        assertNotNull(movies)
        assertEquals(12, viewModel.movieItems.size)
    }

    @Test
    fun getMovie() {
        val movie = viewModel.movie
        assertNotNull(movie)
        assertEquals(movieSelectedSample.title, movie.title)
        assertEquals(movieSelectedSample.cover, movie.cover)
        assertEquals(movieSelectedSample.description, movie.description)
        assertEquals(movieSelectedSample.duration, movie.duration)
        assertEquals(movieSelectedSample.genre, movie.genre)
        assertEquals(movieSelectedSample.release, movie.release)
        assertEquals(movieSelectedSample.score, movie.score)
    }

    @Test
    fun initMoviesData() {
        viewModel.initMoviesData()

        assertNotNull(viewModel.movieItems)
        assertEquals(12, viewModel.movieItems.size)
    }

    @Test
    fun setMovieSelected() {
        viewModel.setMovieSelected(indexSelected)

        val movie = viewModel.movie

        assertNotNull(movie)

        assertEquals(movieSelectedSample.title, movie.title)
        assertEquals(movieSelectedSample.cover, movie.cover)
        assertEquals(movieSelectedSample.description, movie.description)
        assertEquals(movieSelectedSample.duration, movie.duration)
        assertEquals(movieSelectedSample.genre, movie.genre)
        assertEquals(movieSelectedSample.release, movie.release)
        assertEquals(movieSelectedSample.score, movie.score)

    }
}