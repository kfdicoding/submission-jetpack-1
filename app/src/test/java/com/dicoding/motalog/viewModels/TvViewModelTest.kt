package com.dicoding.motalog.viewModels

import com.dicoding.motalog.data.SampleData
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/*
 *
 * skenario test :
 *
 * memuat data tv show:
 *     - memastikan tv show tidak null
 *     - memastikan jumlah data tv show sesuai dengan yang diharapkan
 *     - memastikan data tv show yang dipilih sesuai
 */

class TvViewModelTest {

    private lateinit var viewModel: TvViewModel
    private val tvSelectedSample = SampleData.generateTvData()[0]
    private val indexSelected = 0


    @Before
    fun setUp(){
        viewModel = TvViewModel()
        viewModel.initTvsData()
        viewModel.setTvSelected(indexSelected)
    }

    @Test
    fun getTvItems() {
        val movies = viewModel.tvItems
        Assert.assertNotNull(movies)
        Assert.assertEquals(12, viewModel.tvItems.size)
    }

    @Test
    fun getTv() {
        val tv = viewModel.tv
        Assert.assertNotNull(tv)
        Assert.assertEquals(tvSelectedSample.title, tv.title)
        Assert.assertEquals(tvSelectedSample.cover, tv.cover)
        Assert.assertEquals(tvSelectedSample.description, tv.description)
        Assert.assertEquals(tvSelectedSample.duration, tv.duration)
        Assert.assertEquals(tvSelectedSample.genre, tv.genre)
        Assert.assertEquals(tvSelectedSample.release, tv.release)
        Assert.assertEquals(tvSelectedSample.score, tv.score)
    }

    @Test
    fun initTvsData() {
        viewModel.initTvsData()

        Assert.assertNotNull(viewModel.tvItems)
        Assert.assertEquals(12, viewModel.tvItems.size)
    }

    @Test
    fun setTvSelected() {
        viewModel.setTvSelected(indexSelected)

        val tv = viewModel.tv

        Assert.assertNotNull(tv)

        Assert.assertEquals(tvSelectedSample.title, tv.title)
        Assert.assertEquals(tvSelectedSample.cover, tv.cover)
        Assert.assertEquals(tvSelectedSample.description, tv.description)
        Assert.assertEquals(tvSelectedSample.duration, tv.duration)
        Assert.assertEquals(tvSelectedSample.genre, tv.genre)
        Assert.assertEquals(tvSelectedSample.release, tv.release)
        Assert.assertEquals(tvSelectedSample.score, tv.score)
    }
}