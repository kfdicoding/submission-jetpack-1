package com.dicoding.motalog.fragments

import android.os.SystemClock
import android.view.View
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import com.dicoding.motalog.MainActivity
import com.dicoding.motalog.R
import com.dicoding.motalog.adapters.ItemShowAdapter
import com.dicoding.motalog.data.SampleData
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Test


/*

Skenario test
*   - memastikan ui tampil
*   - memastikan detail movie yang dipilih menampilkan detail movie yang dipilih
*   - memastikan detail tv yang dipilih menampilkan detail tv yang pilih

 */
class FragmentDetailItemShowTest{

    private val indexSelected = 0
    private val movieSelectedSample = SampleData.generateMovieData()[indexSelected]
    private val tvSelectedSample = SampleData.generateTvData()[indexSelected]

    @Before
    fun setUp(){
        ActivityScenario.launch(MainActivity::class.java)
    }

    @Test
    fun tvDetailShow(){
        onView(withId(R.id.mTabLayout)).check(matches(isDisplayed()))
        val matcher: org.hamcrest.Matcher<View>? = Matchers.allOf(
                withText("TV SHOW"),
                isDescendantOfA(withId(R.id.mTabLayout))
        )
        onView(matcher).perform(ViewActions.click())
        SystemClock.sleep(800)

        onView(withId(R.id.mListItemShow)).check(matches(isDisplayed()))
        onView(withId(R.id.mListItemShow)).perform(
                RecyclerViewActions.actionOnItemAtPosition<ItemShowAdapter.ItemShowViewHolder>(indexSelected, ViewActions.click())
        )
        SystemClock.sleep(800)
        //title
        onView(withId(R.id.itemTitle)).check(matches(isDisplayed()))
        onView(withId(R.id.itemTitle)).check(matches(withText(tvSelectedSample.title)))
        //cover
        onView(withId(R.id.itemCover)).check(matches(isDisplayed()))
        //genre
        onView(withId(R.id.itemGenre)).check(matches(isDisplayed()))
        onView(withId(R.id.itemGenre)).check(matches(withText(tvSelectedSample.genre)))
        //popularity
        onView(withId(R.id.itemScore)).check(matches(isDisplayed()))
        onView(withId(R.id.itemScore)).check(matches(withText(tvSelectedSample.score.toString())))
        //duration
        onView(withId(R.id.itemDuration)).check(matches(isDisplayed()))
        onView(withId(R.id.itemDuration)).check(matches(withText(tvSelectedSample.duration)))
        //description
        onView(withId(R.id.itemDescription)).check(matches(isDisplayed()))
        onView(withId(R.id.itemDescription)).check(matches(withText(tvSelectedSample.description)))
    }

    @Test
    fun movieDetailShow(){
        onView(withId(R.id.mTabLayout)).check(matches(isDisplayed()))
        val matcher: org.hamcrest.Matcher<View>? = Matchers.allOf(
                withText("MOVIE"),
                isDescendantOfA(withId(R.id.mTabLayout))
        )
        onView(matcher).perform(ViewActions.click())
        SystemClock.sleep(800)

        onView(withId(R.id.mListItemShow)).check(matches(isDisplayed()))
        onView(withId(R.id.mListItemShow)).perform(
                RecyclerViewActions.actionOnItemAtPosition<ItemShowAdapter.ItemShowViewHolder>(indexSelected, ViewActions.click())
        )
        SystemClock.sleep(800)
        //title
        onView(withId(R.id.itemTitle)).check(matches(isDisplayed()))
        onView(withId(R.id.itemTitle)).check(matches(withText(movieSelectedSample.title)))
        //cover
        onView(withId(R.id.itemCover)).check(matches(isDisplayed()))
        //genre
        onView(withId(R.id.itemGenre)).check(matches(isDisplayed()))
        onView(withId(R.id.itemGenre)).check(matches(withText(movieSelectedSample.genre)))
        //popularity
        onView(withId(R.id.itemScore)).check(matches(isDisplayed()))
        onView(withId(R.id.itemScore)).check(matches(withText(movieSelectedSample.score.toString())))
        //duration
        onView(withId(R.id.itemDuration)).check(matches(isDisplayed()))
        onView(withId(R.id.itemDuration)).check(matches(withText(movieSelectedSample.duration)))
        //description
        onView(withId(R.id.itemDescription)).check(matches(isDisplayed()))
        onView(withId(R.id.itemDescription)).check(matches(withText(movieSelectedSample.description)))
    }
}