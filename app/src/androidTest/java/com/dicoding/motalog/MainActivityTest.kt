package com.dicoding.motalog

import android.os.SystemClock
import android.view.View
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import com.dicoding.motalog.adapters.ItemShowAdapter
import org.hamcrest.Matchers.allOf
import org.junit.Before
import org.junit.Test

/*
*
* Skenario test
*   - memastikan ui tampil
*   - memastikan item movie yang dipilih menampilkan detail movie yang sesuai
*   - memastikan item tv yang dipilih menampilkan detail tv yang sesuai
*
*/


class MainActivityTest{

    @Before
    fun setUp(){
        ActivityScenario.launch(MainActivity::class.java)
    }

    @Test
    fun setUpUI(){
        onView(withId(R.id.mAppBar)).check(matches(isDisplayed()))
        onView(withId(R.id.mToolbar)).check(matches(isDisplayed()))
        onView(withId(R.id.mTabLayout)).check(matches(isDisplayed()))
        onView(withId(R.id.mViewPager)).check(matches(isDisplayed()))
    }

    @Test
    fun tabTvItemSelected(){
        onView(withId(R.id.mTabLayout)).check(matches(isDisplayed()))
        val matcher: org.hamcrest.Matcher<View>? = allOf(
            withText("TV SHOW"),
            isDescendantOfA(withId(R.id.mTabLayout))
        )
        onView(matcher).perform(click())
        SystemClock.sleep(800)

        onView(withId(R.id.mListItemShow)).check(matches(isDisplayed()))
        onView(withId(R.id.mListItemShow)).perform(
            RecyclerViewActions.actionOnItemAtPosition<ItemShowAdapter.ItemShowViewHolder>(0, click())
        )
        SystemClock.sleep(800)
        onView(withId(R.id.itemTitle)).check(matches(isDisplayed()))
        onView(withId(R.id.itemTitle)).check(matches(withText("Arrow")))
    }

    @Test
    fun tabMovieItemSelected(){
        onView(withId(R.id.mTabLayout)).check(matches(isDisplayed()))
        val matcher: org.hamcrest.Matcher<View>? = allOf(
            withText("MOVIE"),
            isDescendantOfA(withId(R.id.mTabLayout))
        )
        onView(matcher).perform(click())
        SystemClock.sleep(800)

        onView(withId(R.id.mListItemShow)).check(matches(isDisplayed()))
        onView(withId(R.id.mListItemShow)).perform(
            RecyclerViewActions.actionOnItemAtPosition<ItemShowAdapter.ItemShowViewHolder>(0, click())
        )
        SystemClock.sleep(800)
        onView(withId(R.id.itemTitle)).check(matches(isDisplayed()))
        onView(withId(R.id.itemTitle)).check(matches(withText("A Star Is Born")))
    }
}